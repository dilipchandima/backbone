var Todo = Backbone.Model.extend({
    default:{
        job:'',
        done:false
    }
});

var Todos = Backbone.Collection.extend({});
var todos = new Todos();

var JobView = Backbone.View.extend({
    model: new Todo,
    tagName:'div',
    events:{
        'click .delete-job':'delete',
        'dblclick .job-text':'edit',
        'click .update-job':'update',
        'enter .job-update':'update',
        'click .cancel-job':'cancel',
        'click .check-job':'checked',
        'click .delete-all':'deleteChecked'
    },
    initialize:function(){
        this.template = _.template($('.job-template').html());
    },
    delete:function(){
        this.model.destroy();
    },
    edit:function(){
        this.$('.delete-job').hide();
        this.$('.edit-job').hide();
        this.$('.update-job').show();
        this.$('.cancel-job').show();

        var job = this.model.get('job');
        this.$('.job').html('<input type="text" class="form-control job-update" value="'+ job +'">');
    },
    update:function(){
        this.model.set('job', $('.job-update').val());
    },
    checked:function(){
        this.model.set('done', !this.model.get('done'));
    },
    cancel:function(){
        jobsview.render();
    },
    render:function(){
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    }
});

var JobsView = Backbone.View.extend({
    model:todos,
    el: $('.jobs-template'),
    initialize:function(){
        this.model.on('add', this.render,this);
        this.model.on('remove',this.render,this);
        this.model.on('change',this.render,this);
    },
    deleteChecked:function(){
        console.log("access fun pare");
        _.each(this.model.toArray(), function(todo){
            console.log("access fun");
            if(todo.get('done')){
                todo.destroy();
                console.log("access");
            }
        });
    },
    render:function(){
        this.$el.html('');
        _.each(this.model.toArray(), function(todo){
            var jobview = new JobView({model:todo});
            this.$('.jobs-template').append(jobview.render().el);
        });
        //var statusView = new StatusView();
        //this.$('.jobs-template').append(statusView.render().el);
        return this;
    }
});

//var StatusView = Backbone.View.extend({
//    model:todos,
//    tagName:'div',
//    initialize:function(){
//        this.template = _.template($('.status-template').html());
//    },
//    render:function(){
//        var done = this.model.length;
//        this.$el.html(this.template({'done':done}));
//        return this;
//    }
//});

var jobsview = new JobsView();

$(document).ready(function(){
    $('.add-job').on('click', function(){
        if($('.input-job').val() !=''){
            var todo = new Todo({
                job:$('.input-job').val(),
                done:false
            });
            todos.add(todo);
            console.log(todo.toJSON());
        }

        $('.input-job').val('');
    });

    $('.delete-all').on('click', function(){
        jobsview.deleteChecked();
    })
});